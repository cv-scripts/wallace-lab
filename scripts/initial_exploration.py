#!/usr/bin/env jupyter
"""
Script/Jupytext notebook to test the exploration of a big multidimensional image

I'd recommend using poetry to install dependencies and manage virtual venvs.
If you are impatient this also works:

pip install dask numpy pandas scikit-image
"""
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: title,-all
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---


# %%
import matplotlib.pyplot as plt
import numpy as np
from dask.array.image import imread
from scipy import ndimage as ndi
from skimage.filters import threshold_otsu
from skimage.morphology import closing, square

bigfile = "/home/alan/Documents/dev/computer_vision_scripting/wallace_lab/Microscopy_Marah_2022-06-01/yET919-SUN4Q570-SRL1CFR610-ASH1CLB2Q670_01_CY5, CY3.5 NAR, CY3, DAPI.tif"

# Load image onto memory using dask
img = imread(bigfile)

# %% Print image shape
print(img.shape)

# %% Plot a single stack

# Plot the last two dimensions (Ellipsis - ... - means "the remanining dimensions")
plt.imshow(img[0, 0, 0, ...])  # The same as img[0, 0 , 0, :,:]
plt.show()

# %% Markdown
"""
By the looks of it the dimensions follow this order: (time, channel, position, x, y)
Also, based on the filename I assume the four channels are CY5, CY3.5 NAR, CY3, DAPI.
"""

# %%

# Let's get rid of that unnecessary first dimension
img = img[0]

# And get one slice with all the channels
four_slices = img[:, 0, ...]
print(four_slices.shape)

# %% Plot all channels for a single cell

# We can reshape without touching the underlying data

# Let us make a 2x2 matrix with our four images
# Split single dimension in four lists and stack them horizontally
reshaped = np.hstack([x for x in four_slices])

# And plot an image across the four dimensions all together (":" means "leave this dim untouched")
plt.imshow(reshaped)
plt.show()

# %% Generate cells using Otsu filter
# More info on (https://scikit-image.org/docs/stable/auto_examples/segmentation/plot_label.html#sphx-glr-auto-examples-segmentation-plot-label-py)

# To generate mock masks I will use the Otsu filters and the second dimension to generate masks defining cells
image_for_masks = four_slices[1].compute()  # compute() enforces loading into memory

# Use otsu method to find a threshold for cells
thresh = threshold_otsu(image_for_masks)

# All pixels above this threshold belong to a cell.
raw_masks = image_for_masks > thresh
plt.imshow(raw_masks)

# Let us clean this up a bit
eroded = ndi.binary_erosion(raw_masks)
eroded = ndi.binary_erosion(eroded)
bw = closing(eroded, square(3))
plt.imshow(bw)
